package com.exam.exammodule04.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.exam.exammodule04.R;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class ApiFragment extends BaseFragment {

    /**
     * Lay json de test
     * http://androidopentutorials.com/android-image-slideshow-using-viewpager/
     *
     * - Library download hinh:
     * https://github.com/nostra13/Android-Universal-Image-Loader
     *
     * https://github.com/square/picasso
     * http://square.github.io/picasso/
     * */

    @Bind(R.id.button1)
    Button btnStart1;
    @Bind(R.id.button2)
    Button btnStart2;
    @Bind(R.id.imageView)
    ImageView img;

    String imgPath = "http://www.bikipcuocsong.com/wp-content/uploads/2014/10/anh-dep-ngoc-trinh-1.png";
    String path2 = "http://prime-force-854.appspot.com/sinhvien/search";
    /*http://prime-force-854.appspot.com/sinhvien/
    + search?name=a
    + getall*/

    @Override
    public int getFragmentId() {
        return R.layout.fragment_api;
    }

    @Override
    public void initView(View view) {

        ButterKnife.bind(this, view);

    }

    @OnClick(R.id.button1)
    public void OnButton1Click() {
        System.out.println("button1 click");
        downImage();
    }

    @OnClick(R.id.button2)
    public void OnButton2Click() {
        timkiemhhs();
    }

    private void downImage() {

        AsyncTask<String, Integer, Boolean> thread = new AsyncTask<String, Integer, Boolean>() {

            Bitmap bm;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                img.setImageBitmap(null);

            }

            @Override
            protected Boolean doInBackground(String... params) {

                try {
                    String path = params[0];

                    URL url = new URL(path);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    //GET / POST / PUT / DELETE
                    connection.setRequestMethod("GET");
                    connection.setConnectTimeout(30000);
                    connection.setReadTimeout(30000);

                    //
                    connection.setDoInput(true);

                    //
                    connection.connect();
                    int status = connection.getResponseCode();

                    if (status ==/*200*/ HttpURLConnection.HTTP_OK) {
                        System.out.println("ket noi thanh cong");

                        int lenght = connection.getContentLength();
                        InputStream in = connection.getInputStream();

                        //c1
                        //bm = BitmapFactory.decodeStream(in);

                        //c2
                        int idx = 0;
                        byte[] arr = new byte[lenght];

                        byte[]buffer = new byte[100];

                        int len = 0;
                        while ( (len = in.read(buffer)) != -1){

                            for (int i = 0; i < len; i++) {
                                arr[idx] = buffer[i];
                                idx++;
                            }

                            publishProgress(idx);
                        }

                        in.close();

                        //
                        bm = BitmapFactory.decodeByteArray(arr, 0, lenght);

                        return true;
                    } else {
                        System.out.println("ket noi khong thanh cong");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return false;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);

                if(result){
                    System.out.println("download hinh thanh cong.");

                    img.setImageBitmap(bm);

                }

            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);

                System.out.println("down: " + values[0]);

            }
        };
        thread.execute( imgPath );

    }

    private void timkiemhhs() {

        AsyncTask<String, Integer, Boolean> thread = new AsyncTask<String, Integer, Boolean>() {

            Bitmap bm;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                img.setImageBitmap(null);

            }

            @Override
            protected Boolean doInBackground(String... params) {

                try {
                    String path = params[0];

                    URL url = new URL(path);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    //GET / POST / PUT / DELETE
                    connection.setRequestMethod("POST");
                    connection.setConnectTimeout(30000);
                    connection.setReadTimeout(30000);

                    //
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    String value = "name=a";
                    OutputStream out = connection.getOutputStream();
                    out.write( value.getBytes() );
                    out.close();

                    //
                    connection.connect();
                    int status = connection.getResponseCode();


                    if (status ==/*200*/ HttpURLConnection.HTTP_OK) {
                        System.out.println("ket noi thanh cong");

                        InputStream in = connection.getInputStream();
                        Scanner sc = new Scanner(in);

                        StringBuffer buffer = new StringBuffer();
                        while(sc.hasNext()){
                            String line = sc.nextLine();
                            buffer.append(line);
                        }

                        sc.close();
                        in.close();

                        //JSONObject json = new JSONObject(buffer.toString());

                        System.out.println(buffer);

                        //
                        return true;
                    } else {
                        System.out.println("ket noi khong thanh cong");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return false;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);

                if(result){
                    System.out.println("get danh sach hoc sinh thanh cong.");
                }

            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);

            }
        };
        thread.execute( path2 );

    }

}
