package com.exam.exammodule04.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by PC14-02 on 1/20/2016.
 */
public class SmsBroadcastReceiver extends BroadcastReceiver{

    //http://developer.android.com/reference/android/provider/Telephony.Sms.Intents.html

    @Override
    public void onReceive(Context context, Intent intent) {

        //System.out.println("sms receiver.");
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            Object objects[] = (Object[]) bundle.get("pdus");
            int phone = bundle.getInt("phone");

            if(objects!=null){
                for (Object obj: objects) {
                    byte[] arr = (byte[]) obj;

                    //
                    SmsMessage sms = SmsMessage.createFromPdu(arr);

                    String msg = sms.getMessageBody();
                    String number = sms.getOriginatingAddress();
                    String address = sms.getDisplayOriginatingAddress();
                    int status = sms.getStatus();

                    System.out.println("msg: " + msg);
                    System.out.println("number: " + number + "; " + address);
                    System.out.println("status: " + status);

                }
            }

        }

    }

}
