package com.exam.exammodule04.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.rtp.AudioStream;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.exam.exammodule04.MainActivity;
import com.exam.exammodule04.R;

import java.io.IOException;

/**
 * Created by PC14-02 on 1/15/2016.
 */
public class AudioService extends Service
{

    public static final String ACTION_UPDATE_SEEKBAR_MEDIA = "mymedia.update.seekbar";

    //https://github.com/Yalantis/CameraModule


    private String names[] = {"Bai hat 1", "Bai hat 2", "Bai hat 3"};
    private int audios[] = { R.raw.kalimba, R.raw.maid_with_theflaxen_hair, R.raw.sleep_away };
    private int audioIndex = 0;

    /**
     * 0 - initPlayer
     * 1- pause
     * 2 - stopMusic
     * */
    public static final String BUNDLE_MUCSIC_KEY = "music_key";

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

        }
    };
    private Runnable myRunnale = new Runnable() {
        @Override
        public void run() {

            sendBroadcastPosIntent.putExtra("max", player.getDuration());
            sendBroadcastPosIntent.putExtra("pos", player.getCurrentPosition());

            sendBroadcast(sendBroadcastPosIntent);

            if (!stop)
            {
                handler.postDelayed(this, 1000);
            }
        }
    };

    private MediaPlayer player;
    boolean stop;
    private final int NOTIFY_ID = 100;

    Intent sendBroadcastPosIntent = new Intent(ACTION_UPDATE_SEEKBAR_MEDIA);

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("service oncreate.");

        registerReceiver(receiverHeadphoneRemove, new IntentFilter(Intent.ACTION_HEADSET_PLUG));

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int key = intent.getIntExtra(BUNDLE_MUCSIC_KEY, 0);
        System.out.println("key - " + key);

        switch (key){
            case 0:

                //play
                if(player==null) {
                    initPlayer();
                }

                if(player.getDuration()<=0){
                    initMeidaPlayer();
                }
                else{
                    playMusic();
                }

                break;
            case 1:
                pauseMusic();
                break;
            case 2:
                //stopMusic
                stopMusic();
                break;
            case 3:
                //next

                if(player!=null){
                    pauseMusic();
                }

                audioIndex++;
                if(audioIndex>=audios.length){
                    audioIndex = 0;
                }

                //
                initMeidaPlayer();

                break;
        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stop = true;

        //
        NotificationManager manager = (NotificationManager) getBaseContext().getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(NOTIFY_ID);

        //
        unregisterReceiver(receiverHeadphoneRemove);

    }

    private void initPlayer(){
        if(player==null){
            player = new MediaPlayer();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                System.out.println("onCompletion");
            }
        });
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                System.out.println("onPrepared");
                //
                playMusic();
            }
        });
        player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                System.out.println("onError");

                return false;
            }
        });
        player.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {

                System.out.println("setOnInfoListener");

                return false;
            }
        });
        player.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                System.out.println("onSeekComplete");
            }
        });

    }

    private void initMeidaPlayer(){
        String packageName = getBaseContext().getPackageName();
        //System.out.println(packageName);
        Uri uri = Uri.parse("android.resource://"+ packageName +"/" + audios[audioIndex]);
        //Uri uri = Uri.parse("http://phuocdat.ucoz.com/2016/mp3/MuaXuanDauTien.mp3");
        System.out.println("init audio index " + audioIndex);
        try {
            player.reset();
            player.setDataSource(getBaseContext(), uri);
            player.prepare();
            //online
            //player.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopMusic(){

        //
        handler.removeCallbacks(myRunnale);

        player.stop();
        player.release();
        player = null;

        NotificationManager manager = (NotificationManager) getBaseContext().getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(NOTIFY_ID);

    }

    private void playMusic(){
        player.start();
        /*SETUP SEEKBAR HANDLE*/
        handler.removeCallbacks(myRunnale);
        handler.postDelayed(myRunnale, 1000);

        //
        postNotification();
    }

    private void pauseMusic(){
        player.pause();
        //remove send broadcast
        handler.removeCallbacks(myRunnale);
    }

    private void postNotification(){

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.owl)
                        .setContentTitle(names[audioIndex])
                        .setTicker(names[audioIndex])
                        .setAutoCancel(false)
                        .setContentText("fasddasfad alsdfjasdl asdl ss asdlasjalf");

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("screenindex", 7);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(getBaseContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager manager = (NotificationManager) getBaseContext().getSystemService(NOTIFICATION_SERVICE);
        manager.notify(NOTIFY_ID, mBuilder.build());

    }

    BroadcastReceiver receiverHeadphoneRemove = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        System.out.println("Headset unplugged");
                        pauseMusic();
                        break;
                    case 1:
                        System.out.println("Headset plugged");
                        break;
                }
            }
        }
    };

}
