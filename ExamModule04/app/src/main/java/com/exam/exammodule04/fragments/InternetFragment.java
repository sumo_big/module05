package com.exam.exammodule04.fragments;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.exam.exammodule04.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class InternetFragment extends BaseFragment{

    @Bind(R.id.textView1)
    TextView textView;
    @Bind(R.id.button1)
    Button button;
    @Bind(R.id.webView1)
    WebView webView;
    @Bind(R.id.progressBar1)
    ProgressBar pbar;


    private String[] url = {
            "http://jakewharton.github.io/butterknife/",
            "http://vnexpress.net/",
            "http://www.phimmoi.net/"
    };

    @Override
    public int getFragmentId() {
        return R.layout.fragment_internet;
    }

    @Override
    public void initView(View view) {

        //
        ButterKnife.bind(this, view);

        //
        setTitle();

        pbar.setVisibility(View.GONE);
        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        boolean val = checkNetworkState();
        if(!val){
            System.out.println("khong ket noi internet");
        }

    }

    private void setTitle(){
        textView.setText("abc");
    }

    public void onClick(){

    }

    @OnClick( {R.id.button1, R.id.button2} )
    public void clickButton(View v){
        System.out.println("click "/* + v.getId()*/);
        if(v.getId()==R.id.button1){
            loadWeb(1);
        }
        else{
            loadWeb(2);
        }
    }

    @OnClick(R.id.button3)
    public void localClick(){
        System.out.println("click 3");

        String hmtl ="<html>\n" +
                "<head>\n" +
                "<title>Page Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "@@@" +
                "<p>This is a paragraph.</p>\n" +
                "\n" +
                "</body>\n" +
                "</html>";

        hmtl = hmtl.replace("@@@", "<a href=\"appModule3://\">open demo Module03</a>");


        webView.loadData(hmtl, "text/html", "utf-8");

    }

    @OnClick(R.id.button4)
    public void downloadClick(){
        download();
    }

    void loadWeb(int index){
        webView.loadUrl(url[index]);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);

                System.out.println("" + newProgress);

            }
        });

        webView.setWebViewClient(new MyWebclient());
    }

    class MyWebclient extends WebViewClient {

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
            //System.out.println(url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            System.out.println("start");
            pbar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            System.out.println("done");
            pbar.setVisibility(View.GONE);
        }
    }

    /**/
    BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /**
             *  kiem tra xem intent chua du lieu ji?
             * */
            System.out.println("network change");
            System.out.println(intent.toString());
            checkNetworkState();
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        //android.net.conn.CONNECTIVITY_CHANGE
        String key = ConnectivityManager.CONNECTIVITY_ACTION;
        getActivity().registerReceiver(networkReceiver, new IntentFilter(key));
        getActivity().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }

    @Override
    public void onPause() {
        super.onPause();

        //
        getActivity().unregisterReceiver(networkReceiver);
        getActivity().unregisterReceiver(onComplete);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private boolean checkNetworkState(){

        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if(info!=null){
            NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if(wifi.isAvailable() && wifi.isConnected()){
                System.out.println("ket noi wifi");
                return true;
            }
            else{
                NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if(mobile.isAvailable() && mobile.isConnected()){
                    System.out.println("ket noi = 3g");
                    return true;
                }
            }
        }

        return false;
    }

    private void download(){

        Uri uri = Uri.parse("https://sohanews2.vcmedia.vn/zoom/660_360/2015/hinh-anh-doremon-che-1433126192951-66-0-428-710-crop-1433150506054.jpg");
        DownloadManager.Request request = new DownloadManager.Request(uri);
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

        //Set the local destination for the downloaded file to a path within the application's external files directory
        request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_PICTURES, "doremon.jpg");
        //

        manager.enqueue(request);
        //request.setAllowedNetworkTypes()
    }

    BroadcastReceiver onComplete=new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            // your code
            System.out.println("download done");



        }
    };

}
