package com.exam.exammodule04.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.View;

import com.exam.exammodule04.R;
import com.exam.exammodule04.services.Service2;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentService extends BaseFragment{

    Service2 mBoundService;
    boolean mServiceBound = false;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_service;
    }

    @Override
    public void initView(View view) {

        //
        View btn = view.findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //start service
                /*Intent intent = new Intent(getActivity(), Service1.class);
                intent.putExtra("value", 100);

                getActivity().startService(intent);*/

                //bind service
                Intent intent = new Intent(getActivity(), Service2.class);
                intent.putExtra("value", 100);

                getActivity().bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

            }
        });



    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServiceBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Service2.MyBinder myBinder = (Service2.MyBinder) service;
            mBoundService = myBinder.getService();
            mServiceBound = true;
        }
    };

}
