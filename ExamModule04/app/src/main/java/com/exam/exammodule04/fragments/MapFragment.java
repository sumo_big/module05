package com.exam.exammodule04.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.exam.exammodule04.R;
import com.exam.exammodule04.adapters.MapInfoAdapter;
import com.exam.exammodule04.constanst.FragmentId;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class MapFragment extends BaseFragment {

    /*
    * listener current location change
    * http://developer.android.com/intl/vi/training/location/receive-location-updates.html
    * http://stackoverflow.com/questions/17591147/how-to-get-current-location-in-android
    *
    * */

    SupportMapFragment mapFragment;
    GoogleMap map;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_menu;
    }

    @Override
    public void initView(View view) {

        ButterKnife.bind(this, view);

        final FragmentManager manager = getChildFragmentManager();
        mapFragment = (SupportMapFragment) manager.findFragmentById(R.id.mapFragment);

        /*
        mapFragment = SupportMapFragment.newInstance();
        FragmentManager manager = getChildFragmentManager();
        //getActivity().getSupportFragmentManager();
        FragmentTransaction tf = manager.beginTransaction();

        tf.replace(R.id.mapLayout, mapFragment);

        tf.commit();*/

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                System.out.println("mapfragment callback");
                //
                map = googleMap;

                //
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                /*if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }*/

                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);
                } else {
                    // Show rationale and request permission.
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1000); //.OnRequestPermissionsResultCallback
                }

                UiSettings settings = map.getUiSettings();
                settings.setCompassEnabled(true);
                //settings.setAllGesturesEnabled(false);
                settings.setMapToolbarEnabled(true);
                settings.setZoomControlsEnabled(true);


                /*7 Đặng Trần Côn, Bến Thành, Quận 1
                Hồ Chí Minh, Vietnam
                10.772282, 106.694037

                Tăng Nhơn Phú B, Quận 9
                Hồ Chí Minh, Vietnam
                10.843445, 106.785857

                Làng trẻ em SOS Gò Vấp, 697 Quang Trung, phường 12, Gò Vấp
                Hồ Chí Minh, Vietnam
                10.839365, 106.645693
                */

                LatLng latLng = new LatLng(10.772282, 106.694037);
                LatLng latLng2 = new LatLng(10.843445, 106.785857);
                LatLng latLng3 = new LatLng(10.839365, 106.645693);
                ArrayList<LatLng>arr = new ArrayList<LatLng>();
                arr.add(latLng);
                arr.add(latLng2);
                arr.add(latLng3);

                LatLngBounds.Builder builder = LatLngBounds.builder();

                for (LatLng ll : arr) {
                    builder.include(ll);

                    //
                    MarkerOptions mrOpt = new MarkerOptions();
                    mrOpt.title("aaa");
                    mrOpt.position(ll);
                    mrOpt.snippet("asldfasdj lasd asdfas ;lfjaakdf asdjs");
                    mrOpt.flat(true);
                    mrOpt.draggable(true);
                    mrOpt.icon( BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET) );

                    //
                    map.addMarker(mrOpt);

                }

                //CameraUpdate cameraUpdate = CameraUpdateFactory.zoomTo(10);
                //CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 20f);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(),0);

                map.animateCamera(cameraUpdate);

                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        System.out.println(latLng.toString());
                    }
                });

                map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        System.out.println(latLng.toString());
                    }
                });

                map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {
                        System.out.println("drag start");
                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {
                        System.out.println("drag " + marker.getPosition().toString());
                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
                        System.out.println("drag end " + marker.getPosition().toString());
                    }
                });

                map.setInfoWindowAdapter(new MapInfoAdapter(getActivity()));

                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        System.out.println("click on infowindow " + marker.getPosition().toString());
                    }
                });

                map.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener() {
                    @Override
                    public void onPolylineClick(Polyline polyline) {
                        System.out.println("line click " + polyline.getId());
                    }
                });

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1000) {
            if (permissions.length > 0 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);
                }

            } else {
                // Permission was denied. Display an error message.

            }

        }
    }

    Polyline line = null;

    @OnClick(R.id.button1)
    public void onDrawLine(){

        /*Lâm Phong, Lý Thái Tổ
        Hồ Chí Minh 760000, Việt Nam
        10.767592, 106.674669

        Ngã sáu Cộng hoà, phường 2, Quận 3
        Hồ Chí Minh, Việt Nam
        10.765432, 106.681428

        36 Lê Hồng Phong, phường 4, Quận 5
        Hồ Chí Minh, Việt Nam
        10.762149, 106.676445
        */

        if(line!=null){
            line.remove();
            line = null;
        }
        else {
            PolylineOptions lines = new PolylineOptions();
            lines.add(new LatLng(10.767592, 106.674669));
            lines.add(new LatLng(10.765432, 106.681428));
            lines.add(new LatLng(10.762149, 106.676445));

            lines.color(Color.RED);

            lines.width(10);

            line = map.addPolyline(lines);
            line.setClickable(true);

            System.out.println("draw line");
        }

    }

    @OnClick(R.id.button2)
    public void onDrawPolygon(){
        PolygonOptions lines = new PolygonOptions();
        lines.add(new LatLng(10.767592, 106.674669));
        lines.add(new LatLng(10.765432, 106.681428));
        lines.add(new LatLng(10.762149, 106.676445));

        lines.fillColor(Color.argb(60, 150, 150, 150));

        lines.strokeWidth(10);

        map.addPolygon(lines);
    }

    @OnClick(R.id.button3)
    public void onDrawCircle(){

        //cac ban tu ve hinh tron.
        /*
        * Ground Overlays -> doi tuong ve bitmap len ban do tai vi tri nao do.
        * Utility Library: bộ thư viện tính toán khoảng cách giữa 2 điểm.
        * */

    }

}
