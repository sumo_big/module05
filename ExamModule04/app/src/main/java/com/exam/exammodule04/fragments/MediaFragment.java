package com.exam.exammodule04.fragments;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.exam.exammodule04.MainActivity;
import com.exam.exammodule04.R;
import com.exam.exammodule04.module.TaskDowload;
import com.exam.exammodule04.services.AudioService;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class MediaFragment extends BaseFragment{

    private final String MUSIC_SERVICE_ACTION = "com.exam.music.service";

    @Bind(R.id.seekBar)
    SeekBar sbar;
    @Bind(R.id.surfaceView)
    SurfaceView sfView;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_media;
    }

    @Override
    public void initView(View view) {

        ButterKnife.bind(this, view);


    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().registerReceiver(receiverSeekbar, new IntentFilter(AudioService.ACTION_UPDATE_SEEKBAR_MEDIA));

    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(receiverSeekbar);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @OnClick(R.id.button1)
    public void onPlayClick(){
        Intent intent = new Intent(getActivity(), AudioService.class);
        intent.putExtra(AudioService.BUNDLE_MUCSIC_KEY, 0);

        getActivity().startService(intent);
    }

    @OnClick(R.id.button2)
    public void onPauseClick(){
        Intent intent = new Intent(getActivity(), AudioService.class);
        intent.putExtra(AudioService.BUNDLE_MUCSIC_KEY, 1);

        getActivity().startService(intent);
    }

    @OnClick(R.id.button3)
    public void onStopClick(){
        Intent intent = new Intent(getActivity(), AudioService.class);
        intent.putExtra(AudioService.BUNDLE_MUCSIC_KEY, 2);

        getActivity().startService(intent);
    }

    @OnClick(R.id.button4)
    public void onNext(){
        Intent intent = new Intent(getActivity(), AudioService.class);
        intent.putExtra(AudioService.BUNDLE_MUCSIC_KEY, 3);

        getActivity().startService(intent);
    }

    BroadcastReceiver receiverSeekbar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int max = intent.getIntExtra("max", 0);
            int curPos = intent.getIntExtra("pos", 0);

            System.out.println("max: " + max + "; curr: " + curPos);

            sbar.setMax(max);
            sbar.setProgress(curPos);

        }
    };

    /*------------------------------------*/

    MediaPlayer player;

    @OnClick(R.id.button1_1)
    public void playVideo(){
        if(player==null){
            player = new MediaPlayer();

            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    System.out.println("onPrepared");

                    player.start();

                }
            });
        }

        Uri uri = Uri.parse("android.resource://"+ getActivity().getPackageName() +"/" + R.raw.video);

        try {
            player.reset();

            player.setSurface(sfView.getHolder().getSurface());

            player.setDataSource(getActivity(), uri);
            player.prepare();

            //online
            //player.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    @OnClick(R.id.button5)
    public void volumeUp(){

        AudioManager manager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        int volume = manager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volume++;
        manager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);

    }

    @OnClick(R.id.button6)
    public void volumeDown(){
        AudioManager manager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        int volume = manager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volume--;
        if(volume<0)
        {
            volume=0;
        }
        manager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
    }

    boolean mute = false;
    @OnClick(R.id.button7)
    public void volumeMute(){
        AudioManager manager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        //boolean val = manager.isStreamMute(AudioManager.STREAM_MUSIC);
        mute = !mute;
        manager.setStreamMute(AudioManager.STREAM_MUSIC, mute);
    }

}
