package com.exam.exammodule04.fragments;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Telephony;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.view.View;

import com.exam.exammodule04.R;
import com.exam.exammodule04.constanst.FragmentId;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class SmsCallFragment extends BaseFragment{
    public static final String ACTION_SMS_SEND = "myapp.send.sms";
    public static final String ACTION_SMS_DELIVERY = "myapp.delivery.sms";

    /**
     * Action sms receiver:
     *  + android.provider.Telephony.SMS_RECEIVED
     *  + Telephony.Sms.Intents.SMS_RECEIVED_ACTION
     *
     * Call
     * http://www.truiton.com/2014/08/android-phonestatelistener-example/
     * */

    @Override
    public int getFragmentId() {
        return R.layout.fragment_smscall;
    }

    @Override
    public void initView(View view) {
        ButterKnife.bind(this, view);

        //Telephony.Sms.Intents.SMS_RECEIVED_ACTION
    }

    BroadcastReceiver receiverSendSms = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = getResultCode();
            //Activity.RESULT_OK
            System.out.println("sms send success " + status);
        }
    };

    BroadcastReceiver receiverDeliverySms = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = getResultCode();
            //Activity.RESULT_OK
            System.out.println("sms receive success " + status);
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        getActivity().registerReceiver(receiverSendSms, new IntentFilter(ACTION_SMS_SEND));
        getActivity().registerReceiver(receiverDeliverySms, new IntentFilter(ACTION_SMS_DELIVERY));

        /*call listener*/
        TelephonyManager manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = manager.getDeviceId();
        int states = manager.getDataState();
        String phoneNumber = manager.getLine1Number();
        String iso = manager.getNetworkCountryIso();
        String operator = manager.getNetworkOperator();

        switch (states){
            case TelephonyManager.DATA_CONNECTED:
                break;
        }

        System.out.println("deviceId: " + deviceId);
        System.out.println("phone:" + phoneNumber);
        System.out.println("iso" + iso);
        System.out.println("operator:" + operator);

        manager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

    }

    PhoneStateListener phoneStateListener = new PhoneStateListener(){
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);

            switch (state){
                case TelephonyManager.CALL_STATE_IDLE:
                    System.out.println("idle - off");
                    break;

                case TelephonyManager.CALL_STATE_OFFHOOK:
                    System.out.println("offhook - nghe");
                    break;

                case TelephonyManager.CALL_STATE_RINGING:
                    System.out.println("ringing");
                    break;
            }

        }
    };

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(receiverSendSms);
        getActivity().unregisterReceiver(receiverDeliverySms);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @OnClick(R.id.button1)
    public void sendSms(){

        String msg = "heloooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo";

        SmsManager manager = SmsManager.getDefault();
        List<String> arrSms = manager.divideMessage(msg);

        Intent send = new Intent(ACTION_SMS_SEND);
        PendingIntent sendPending = PendingIntent.getBroadcast(getContext(), 0, send, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent delivery = new Intent(ACTION_SMS_DELIVERY);
        PendingIntent deliveryPending = PendingIntent.getBroadcast(getContext(), 0, delivery, PendingIntent.FLAG_UPDATE_CURRENT);

        manager.sendTextMessage("5554", null, msg, sendPending, deliveryPending);
        //manager.sendMultipartTextMessage();

        //System.out.println("send sms");

    }

}
