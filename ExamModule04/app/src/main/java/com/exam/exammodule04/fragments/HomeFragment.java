package com.exam.exammodule04.fragments;

import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.exam.exammodule04.MainActivity;
import com.exam.exammodule04.R;
import com.exam.exammodule04.adapters.HomeAdapter;
import com.exam.exammodule04.constanst.FragmentId;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class HomeFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    ListView listView;
    GridView gridView;
    View btn1, btn2;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initView(View view) {
        listView = (ListView) view.findViewById(R.id.listView);
        gridView = (GridView) view.findViewById(R.id.gridView);
        btn1 = view.findViewById(R.id.button1);
        btn2 = view.findViewById(R.id.button2);

        HomeAdapter adapter = new HomeAdapter(getActivity());
        listView.setAdapter(adapter);
        gridView.setAdapter(adapter);

        btn1.setEnabled(false);
        btn2.setEnabled(true);
        listView.setVisibility(View.VISIBLE);
        gridView.setVisibility(View.GONE);

        listView.setOnItemClickListener(this);
        gridView.setOnItemClickListener(this);

        View.OnClickListener onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == btn1){
                    btn1.setEnabled(false);
                    btn2.setEnabled(true);
                    listView.setVisibility(View.VISIBLE);
                    gridView.setVisibility(View.GONE);
                }
                else{
                    btn1.setEnabled(true);
                    btn2.setEnabled(false);
                    listView.setVisibility(View.GONE);
                    gridView.setVisibility(View.VISIBLE);
                }
            }
        };

        btn1.setOnClickListener(onClick);
        btn2.setOnClickListener(onClick);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        MainActivity activity = (MainActivity) getActivity();

        switch (position){
            case 0:
                //Animation
                activity.addFragment(FragmentId.INTERNET, null, true);
                break;
            case 1:
                //json-xml
                activity.addFragment(FragmentId.JSONXML, null, true);
                break;

            case 2:
                //api
                activity.addFragment(FragmentId.API, null, true);
                break;

            case 3:
                //map
                activity.addFragment(FragmentId.MAP, null, true);
                break;

            case 4:
                //map2
                activity.addFragment(FragmentId.ACTIONBAR, null, true);
                break;

            case 5:
                //gcm
                activity.addFragment(FragmentId.GCM, null, true);
                break;

            case 6:
                //media
                activity.addFragment(FragmentId.MEDIA, null, true);
                break;

            case 8:
                //smscall
                activity.addFragment(FragmentId.SmsCall, null, true);
                break;

        }

    }
}
