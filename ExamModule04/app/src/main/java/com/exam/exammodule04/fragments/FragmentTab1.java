package com.exam.exammodule04.fragments;

import android.view.View;
import android.widget.ListView;

import com.exam.exammodule04.R;
import com.exam.exammodule04.adapters.HocsinhAdapter;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class FragmentTab1 extends BaseFragment{

    ListView listView;
    HocsinhAdapter adapter;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_tab1;
    }

    @Override
    public void initView(View view) {

        //
        setHasOptionsMenu(true);

        listView = (ListView) view.findViewById(R.id.listView);
        adapter = new HocsinhAdapter(getActivity());
        listView.setAdapter(adapter);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
