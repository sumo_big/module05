package com.exam.exammodule04.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.exam.exammodule04.R;
import com.exam.exammodule04.mygcm.QuickstartPreferences;
import com.exam.exammodule04.mygcm.RegistrationIntentService;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by PC14-02 on 11/23/2015.
 */
public class GCMFragment extends BaseFragment{

    @Bind(R.id.textView2)
    TextView tvToken;
    @Bind(R.id.textView3)
    TextView tvStatus;

    @Bind(R.id.button1)
    Button btnRegister;

    BroadcastReceiver receiverGCM = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("receiver cgcm");
            checkGcm();
        }
    };

    @Override
    public int getFragmentId() {
        return R.layout.fragment_gcm;
    }

    @Override
    public void initView(View view) {

        ButterKnife.bind(this, view);

        //
        checkGcm();

    }

    private void checkGcm(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String token = sharedPreferences.getString(QuickstartPreferences.GCM_TOKEN, null);
        System.out.println("GCM Token: " + token);

        if(token==null) {
            tvToken.setText("Token: null");
            tvStatus.setText("Chua dang ky GCM");
            btnRegister.setEnabled(true);
        }
        else{
            tvToken.setText("Token: " + token);
            tvStatus.setText("Da dang ky GCM");
            btnRegister.setEnabled(false);

            boolean sendToSever = sharedPreferences.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
            if(!sendToSever){
                //send token to server
                showToast("chua dua token len server");
            }
            else{
                showToast("da dua token len server roi");
            }
        }
    }

    @OnClick(R.id.button1)
    public void onRegister(){
        Intent intentGcm = new Intent(getActivity(), RegistrationIntentService.class);
        getActivity().startService(intentGcm);
    }

    @OnClick(R.id.button2)
    public void onClearCache(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        sharedPreferences.edit().remove(QuickstartPreferences.GCM_TOKEN).apply();

        //
        checkGcm();
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().registerReceiver(receiverGCM, new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiverGCM);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
