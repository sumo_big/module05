package com.exam.exammodule04;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.exam.exammodule04.constanst.FragmentId;
import com.exam.exammodule04.fragments.ActionbarDropdownFragment;
import com.exam.exammodule04.fragments.ActionbarFragment;
import com.exam.exammodule04.fragments.ActionbarTabsFragment;
import com.exam.exammodule04.fragments.ApiFragment;
import com.exam.exammodule04.fragments.InternetFragment;
import com.exam.exammodule04.fragments.BaseFragment;
import com.exam.exammodule04.fragments.GCMFragment;
import com.exam.exammodule04.fragments.SmsCallFragment;
import com.exam.exammodule04.fragments.FragmentPagerView;
import com.exam.exammodule04.fragments.MediaFragment;
import com.exam.exammodule04.fragments.HomeFragment;
import com.exam.exammodule04.fragments.JsonXmlFragment;
import com.exam.exammodule04.fragments.MenuActionModeFragment;
import com.exam.exammodule04.fragments.MenuContextFragment;
import com.exam.exammodule04.fragments.MapFragment;
import com.exam.exammodule04.mygcm.QuickstartPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity {

    /**
     * Google App: k217
     * - Server Key: AIzaSyBmFrie937o0TwgPl6oZAA_idzPdiHGNI8
     * - Android Key: AIzaSyDIDccPYrP-Y6SXHdQlC6ArdcqUz2k4DyY
     * */
    /*data test GCM*/
    /*{
        "to" : "ci6TrJPirOU:APA91bHsKRH-5NID4pTl0XZ1AxZ_XqwORQTsx_KFPWqQiNl66nyRxkVkHxuWi0HH1FEToXs_EkJMucvOadDZ6Ghiun77qsZqEy6Jto4cOyiAvwPau4J9Q-AjufB4DP8Q-vmhyqc10G_V",
            "data" : {
        "message":"Nhau thay Nga oi",
                "key1":123
    }
    }*/

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        boolean notificaton = false;
        Intent intent = getIntent();
        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                String key = bundle.getString("data1");
                System.out.println("data1:" + key);
                if(key!=null){
                    notificaton = true;
                }
            }
        }

        //
        if(notificaton){
            addFragment(FragmentId.NOTIFICATION, null, false);
        }
        else {
            addFragment(FragmentId.HOME, null, false);
        }
        //ktra db co ton tai hay chua? -> copy vao local

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String token = sharedPreferences.getString(QuickstartPreferences.GCM_TOKEN, null);
        System.out.println("GCM Token: " + token);

        if(token==null) {
            if (checkPlayServices()) {
                // Start IntentService to register this application with GCM.
                /*Intent intentGcm = new Intent(this, RegistrationIntentService.class);
                startService(intentGcm);*/
            }
            else{
                System.out.println("Khong co goolge play service trong may");
            }
        }
        else{
            boolean sendToSever = sharedPreferences.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
            if(!sendToSever){
                //send token to server
            }
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {

        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                String key = bundle.getString("data1");
                System.out.println("data1:" + key);
            }
        }

    }

    public void addFragment(FragmentId id, Bundle bundle, boolean addToStack){

        BaseFragment fragment = null;
        if(id == FragmentId.HOME){
            fragment = new HomeFragment();
        }
        else if(id==FragmentId.JSONXML){
            fragment = new JsonXmlFragment();
        }
        else if(id==FragmentId.API){
            fragment = new ApiFragment();
        }
        else if(id==FragmentId.INTERNET){
            fragment = new InternetFragment();
        }
        else if(id==FragmentId.MAP){
            fragment = new MapFragment();
        }
        else if(id==FragmentId.MENU_FLOAT_CONTEXT){
            fragment = new MenuContextFragment();
        }
        else if(id==FragmentId.MENU_ACTIONMODE){
            fragment = new MenuActionModeFragment();
        }
        else if(id==FragmentId.ACTIONBAR){
            fragment = new ActionbarFragment();
        }
        else if(id==FragmentId.ACTIONBAR_DROPDOWN){
            fragment = new ActionbarDropdownFragment();
        }
        else if(id==FragmentId.ACTIONBAR_TABS){
            fragment = new ActionbarTabsFragment();
        }
        else if(id==FragmentId.PAGERVIEW){
            fragment = new FragmentPagerView();
        }
        else if(id==FragmentId.GCM){
            fragment = new GCMFragment();
        }
        else if(id==FragmentId.MEDIA){
            fragment = new MediaFragment();
        }
        else if(id==FragmentId.SmsCall){
            fragment = new SmsCallFragment();
        }
        /*else if(id==FragmentId.NOTIFICATION){
            fragment = new FragmentNotification();
        }
        else if(id==FragmentId.BROADCASTRECEIVER){
            fragment = new FragmentBroadcastReceiver();
        }
        else if(id==FragmentId.SERVICE){
            fragment = new FragmentService();
        }*/

        fragment.setArguments(bundle);

        FragmentManager manager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ts = manager.beginTransaction();
        /*ts.setCustomAnimations(R.animator.slide_in_right,
                R.animator.slide_out_left,
                R.animator.slide_in_left,
                R.animator.slide_out_right);*/

        ts.setCustomAnimations(R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right);

        ts.replace(R.id.contain, fragment);
        if(addToStack){
            ts.addToBackStack(id.getKey());
        }

        //
        ts.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_main, menu);

        //
        MenuItem searchItem = menu.findItem(R.id.searchItem);
        /*
        * 3.0
        * */
        //searchItem.getsearchView();

        /*
        * v7 support
        * */
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    System.out.println(newText);
                    return true;
                }
            });

            searchView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode==KeyEvent.KEYCODE_ENTER){
                        System.out.println("enter");
                    }
                    return false;
                }
            });
        }

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_home:
                System.out.println("item home click");
                break;
            case R.id.menu_setting:
                System.out.println("item setting click");
                break;
            case R.id.menu_product:
                System.out.println("item product click");
                break;
        }

        return false;
    }
}