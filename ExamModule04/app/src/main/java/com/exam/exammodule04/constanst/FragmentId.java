package com.exam.exammodule04.constanst;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public enum FragmentId {

    HOME("home"),
    INTERNET("INTERNET"),
    JSONXML("sqlite"),
    API("API"),
    MAP("menu"),
    MENU_FLOAT_CONTEXT("menu_float"),
    MENU_ACTIONMODE("menu_actionmode"),
    ACTIONBAR("actionbar"),
    ACTIONBAR_DROPDOWN("actionbar_dropdown"),
    ACTIONBAR_TABS("actionbar_tabs"),
    PAGERVIEW("pagerview"),
    TABS_PAGERVIEW("tab_pagerview"),
    GCM("GCM"),
    MEDIA("MEDIA"),
    SmsCall("SmsCall"),
    BROADCASTRECEIVER("BROADCAST"),
    NOTIFICATION("NOTIFICATION"),
    SERVICE("SERVICE");

    private String key;

    private FragmentId(String key){
        this.key = key;
    }

    public String getKey(){
        return key;
    }

}
