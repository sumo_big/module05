package com.exam.exammodule04.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.exam.exammodule04.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by PC14-02 on 1/11/2016.
 */
public class MapInfoAdapter implements GoogleMap.InfoWindowAdapter{

    Context context;

    public MapInfoAdapter(Context ct) {
        context =ct;
    }

    @Override
    public View getInfoWindow(Marker marker) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_item_infowindow, null, false);

        TextView tv = (TextView) view.findViewById(R.id.textView1);
        TextView tv2 = (TextView) view.findViewById(R.id.textView2);

        tv.setText(marker.getTitle());
        tv2.setText(marker.getSnippet());

        return view;

        //return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        /*LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_item_infowindow, null, false);

        TextView tv = (TextView) view.findViewById(R.id.textView1);
        TextView tv2 = (TextView) view.findViewById(R.id.textView2);

        tv.setText(marker.getTitle());
        tv2.setText(marker.getSnippet());

        return view;*/
        return null;
    }

}
