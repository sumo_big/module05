package com.exam.exammodule04.fragments;

import android.content.res.AssetManager;
import android.util.Xml;
import android.view.View;

import com.exam.exammodule04.R;
import com.exam.exammodule04.module.HocSinh;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.InputStream;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class JsonXmlFragment extends BaseFragment{

    @Override
    public int getFragmentId() {
        return R.layout.fragment_jsonxml;
    }

    @Override
    public void initView(View view) {

        //
        ButterKnife.bind(this, view);


    }

    @OnClick(R.id.button1)
    public void parseJson(){

        AssetManager manager = getActivity().getAssets();
        try {

            InputStream in = manager.open("json_sinhvien.json");

            //
            Scanner scanner = new Scanner(in);
            StringBuffer buffer = new StringBuffer();

            while (scanner.hasNext()){
                String line = scanner.nextLine();
                buffer.append(line);
            }
            //
            scanner.close();

            //
            //System.out.println(buffer);

            //
            JSONObject json = new JSONObject(buffer.toString());
            String title = json.getString("title");
            String method = json.getString("method");
            JSONArray jArr = json.getJSONArray("array");

            System.out.println(title);
            System.out.println(method);

            Gson gson = new Gson();


            for (int i = 0; i < jArr.length(); i++) {
                JSONObject jhs = jArr.getJSONObject(i);
                /*String name = jhs.getString("name");
                int year = jhs.getInt("year");

                System.out.println(name);
                System.out.println(year);

                HocSinh hs = new HocSinh();
                hs.ten = name;
                hs.namsinh = year;*/

                HocSinh hs = gson.fromJson(jhs.toString(), HocSinh.class);

                if(hs!=null){
                    System.out.println(hs.name);
                    System.out.println(hs.year);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.button2)
    public void parseXml(){

        try {
            AssetManager manager = getActivity().getAssets();
            InputStream in = manager.open("xml_sinhvien.xml");

            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in, null);
                int tag = parser.nextTag();
                if(tag == XmlPullParser.START_TAG){
                    String name = parser.getName();
                    if(name.equalsIgnoreCase("danhsach")){
                        System.out.println(tag);
                        System.out.println(name);

                        //
                        tag = parser.nextTag();

                        while (tag==XmlPullParser.START_TAG){

                            /*if(tag!=XmlPullParser.START_TAG){
                                continue;
                            }*/

                            name = parser.getName();
                            if(name.equalsIgnoreCase("title")){
                                parserText(parser);
                            }
                            else if(name.equalsIgnoreCase("method")){
                                parserText(parser);
                            }
                            else if(name.equalsIgnoreCase("hocsinh")){
                                parserStudent(parser);
                            }

                            tag = parser.nextTag();

                        }

                    }
                }

            } finally {
                in.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String parserText(XmlPullParser parser){
        try {
            String value = "";
            int tag = parser.next();
            while(tag!=XmlPullParser.END_TAG){
                if(tag==XmlPullParser.TEXT){
                    value = parser.getText();
                    System.out.println(value);
                }
                tag = parser.nextTag();
            }
            return  value;
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void parserStudent(XmlPullParser parser){
        try {
            HocSinh hs = new HocSinh();
            int tag = parser.nextTag();
            while(tag!=XmlPullParser.END_TAG){
                String nameTag = parser.getName();
                if(nameTag.equalsIgnoreCase("name")){
                    String name = parserText(parser);
                    //System.out.println(name);
                    hs.name = name;
                }
                else if(nameTag.equalsIgnoreCase("year")){
                    String year = parserText(parser);
                    //System.out.println(year);
                    hs.year = Integer.parseInt(year);
                }
                tag = parser.nextTag();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.button3)
    public void domParser(){

        try {
            AssetManager manager = getActivity().getAssets();
            InputStream in = manager.open("xml_sinhvien.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(in);

            //Node
            //Element

            Element root = doc.getDocumentElement();
            System.out.println(root.getNodeName());

            NodeList titleList = root.getElementsByTagName("title");
            int n = titleList.getLength();
            for (int i = 0; i < n; i++) {
                Element title = (Element) titleList.item(i);
                System.out.println(title.getNodeName());
                System.out.println(title.getNodeValue());
            }

            //
            NodeList hsList = root.getElementsByTagName("hocsinh");
            n = hsList.getLength();
            for (int i = 0; i < n; i++) {
                Node hs =  hsList.item(i);
                NodeList hsSub = hs.getChildNodes();
                for (int j = 0; j < hsSub.getLength(); j++) {
                    Node node = hsSub.item(j);
                    System.out.println(node.getNodeName());
                    System.out.println(node.getNodeValue());
                }
            }


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
